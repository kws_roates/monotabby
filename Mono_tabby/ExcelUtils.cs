﻿using System;

namespace Mono_tabby
{
    class ExcelUtils
    {
        public static int ColumnLetterToColumnIndex(string columnLetter)
        {
            columnLetter = columnLetter.ToUpper();
            int sum = 0;

            for (int i = 0; i < columnLetter.Length; i++)
            {
                sum *= 26;
                sum += (columnLetter[i] - 'A' + 1);
            }
            return sum;
        }

        public static string ColumnIndexToColumnLetter(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = string.Empty;
            int modulo;

            if (dividend == -1)
            {
                columnName = "N/A";
            }
            else
            {
                while (dividend > 0)
                {
                    modulo = (dividend - 1) % 26;
                    columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                    dividend = (dividend - modulo) / 26;
                }
            }

            return columnName;
        }
    }
    
}
