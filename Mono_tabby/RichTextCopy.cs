﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Mono_tabby
{
    //my new onion code
    public class RichTextCopy
    {
        private List<ColorIndex> colorLists;

        private ExcelPackage epplus_excel_src;
        private ExcelPackage epplus_excel_trg;
        private HSSFWorkbook hssf_excel;


        private enum Excel_to_xls { XlsxtoXlsx, XlsxToXls2003, Xls2003ToXlsx };

        private readonly Excel_to_xls mode;
        //xlsx to xlsx
        public RichTextCopy(ExcelPackage xlsx_src, ExcelPackage xlsx_trg)
        {
            mode = Excel_to_xls.XlsxtoXlsx;
            epplus_excel_src = xlsx_src;
            epplus_excel_trg = xlsx_trg;
            Console.WriteLine("Xlsx->Xlsx");
        }

        //xlsx to xls
        public RichTextCopy(ExcelPackage xlsx_src, HSSFWorkbook xls_trg)
        {
            mode = Excel_to_xls.XlsxToXls2003;

            epplus_excel_src = xlsx_src;
            hssf_excel = xls_trg;

            List<ColorIndex> colorList = new List<ColorIndex>();
            colorLists = colorList;

            Console.WriteLine("Xlsx->Xls2003");
        }

        //xls to xlsx
        public RichTextCopy(HSSFWorkbook xls_src, ExcelPackage xlsx_trg)
        {
            mode = Excel_to_xls.Xls2003ToXlsx;

            hssf_excel = xls_src;
            epplus_excel_trg = xlsx_trg;


            List<ColorIndex> colorList = new List<ColorIndex>();
            colorLists = colorList;

            Console.WriteLine("Xls2003->Xlsx");
        }

        public void CopyCell(string worksheetname_src, string worksheetname_trg, int src_row, int src_col, int trg_row, int trg_col)
        {

            switch (mode)
            {
                case Excel_to_xls.XlsxtoXlsx:
                    XlsxtoXlsx(worksheetname_src, worksheetname_trg, src_row, src_col, trg_row, trg_col);
                    break;
                case Excel_to_xls.XlsxToXls2003:
                    XlsxToXls2003(worksheetname_src, worksheetname_trg, src_row, src_col, trg_row, trg_col);
                    break;
                case Excel_to_xls.Xls2003ToXlsx:
                    Xls2003ToXlsx(worksheetname_src, worksheetname_trg, src_row, src_col, trg_row, trg_col);
                    break;
                default:
                    break;
            }

        }

        private void XlsxtoXlsx(string worksheetname_src, string worksheetname_trg, int src_row, int src_col, int trg_row, int trg_col)
        {
            /*wip*/


            ExcelWorksheet ws_src = epplus_excel_src.Workbook.Worksheets[worksheetname_src];

            ExcelWorksheet ws_trg = epplus_excel_trg.Workbook.Worksheets[worksheetname_trg];

            string font_color = ws_src.Cells[src_row, src_col].Style.Font.Color.Rgb;

            string fill_bkcolor = ws_src.Cells[src_row, src_col].Style.Fill.BackgroundColor.Rgb;

            string pattern_color = ws_src.Cells[src_row, src_col].Style.Fill.PatternColor.Rgb;

            //2020-12-18 extra feature added keep color of cell if it exits in the target
            bool target_has_color = false;
            string target_bkcolor = ws_trg.Cells[trg_row, trg_col].Style.Fill.BackgroundColor.Rgb;
            //if(ws_trg.Cells[trg_row, trg_col].Style.Fill.BackgroundColor.Rgb != null)
            //{
            //    target_bkcolor = ws_trg.Cells[trg_row, trg_col].Style.Fill.BackgroundColor.Rgb;
            //}

            string target_pattern_color = ws_trg.Cells[trg_row, trg_col].Style.Fill.PatternColor.Rgb;

            //if (ws_trg.Cells[trg_row, trg_col].Style.Fill.BackgroundColor.LookupColor() != "#FF000000" && ws_trg.Cells[trg_row, trg_col].Style.Fill.PatternType.ToString() != "None")
            //{
            //    target_has_color = true;
            //}

            if (ws_trg.Cells[trg_row, trg_col].Style.Fill.PatternType.ToString() != "None")
            {
                target_has_color = true;
            }

            //intrinsic font, size, bold, underline, italic, strikethrough 
            string fontname_cell = ws_src.Cells[src_row, src_col].Style.Font.Name;
            float size_cell = ws_src.Cells[src_row, src_col].Style.Font.Size;
            bool bold_cell = ws_src.Cells[src_row, src_col].Style.Font.Bold;
            bool underline_cell = ws_src.Cells[src_row, src_col].Style.Font.UnderLine;
            bool italic_cell = ws_src.Cells[src_row, src_col].Style.Font.Italic;
            bool strikethrough_cell = ws_src.Cells[src_row, src_col].Style.Font.Strike;




            string textvalue = "";
            if (ws_src.Cells[src_row, src_col].Value != null)
            {
                textvalue = ws_src.Cells[src_row, src_col].Value.ToString();
                textvalue = textvalue.Replace("\t", " ").Replace("\r", "").TrimEnd();
            }


            int richTextCount = ws_src.Cells[src_row, src_col].RichText.Count;

            if (ws_src.Cells[src_row, src_col].IsRichText == true)
            {
                //Console.WriteLine("Rich Text src_color found");

                //clear the cell
                ws_trg.Cells[trg_row, trg_col].Value = null;
                ws_trg.Cells[trg_row, trg_col].RichText.Clear();
                ws_trg.Cells[trg_row, trg_col].IsRichText = true;
                //int richTextElementIndex = 0;

                if (fill_bkcolor != null)
                {
                    if (fill_bkcolor != "")
                    {
                        ws_trg.Cells[trg_row, trg_col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        ws_trg.Cells[trg_row, trg_col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml($"#{fill_bkcolor}").A, ColorTranslator.FromHtml($"#{fill_bkcolor}").R, ColorTranslator.FromHtml($"#{fill_bkcolor}").G, ColorTranslator.FromHtml($"#{fill_bkcolor}").B);
                    }

                }

                for (int i = 0; i < richTextCount; i++)
                {

                    //text properties from epplus
                    Color epplusColor = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).Color;
                    bool epplusBold = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).Bold;
                    string epplusFontname = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).FontName;
                    float epplusSize = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).Size;
                    bool epplusUnderline = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).UnderLine;
                    bool epplusItalic = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).Italic;
                    bool epplusStrike = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).Strike;
                    ExcelVerticalAlignmentFont epplusValign = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).VerticalAlign;
                    //bool epplusPspace = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).PreserveSpace;
                    string epplusText = ws_src.Cells[src_row, src_col].RichText.ElementAt(i).Text;

                    if (epplusSize == 0)
                    {
                        epplusSize = 11;
                    }


                    //assign the font attributes to the target epplus object
                    ws_trg.Cells[trg_row, trg_col].RichText.Insert(i, epplusText);
                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).Color = epplusColor;
                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).Bold = epplusBold;
                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).FontName = epplusFontname;
                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).Size = epplusSize;
                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).UnderLine = epplusUnderline;
                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).Italic = epplusItalic;
                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).Strike = epplusStrike;



                    switch (epplusValign)
                    {
                        case ExcelVerticalAlignmentFont.None:
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).VerticalAlign = ExcelVerticalAlignmentFont.None;
                            break;
                        case ExcelVerticalAlignmentFont.Baseline:
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).VerticalAlign = ExcelVerticalAlignmentFont.Baseline;
                            break;
                        case ExcelVerticalAlignmentFont.Subscript:
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).VerticalAlign = ExcelVerticalAlignmentFont.Subscript;
                            break;
                        case ExcelVerticalAlignmentFont.Superscript:
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(i).VerticalAlign = ExcelVerticalAlignmentFont.Superscript;
                            break;
                        default:
                            break;
                    }


                }

            }
            else
            {


                ws_trg.Cells[trg_row, trg_col].Value = textvalue;

                if (font_color != null && font_color != "")
                {
                    ws_trg.Cells[trg_row, trg_col].Style.Font.Color.SetColor(ColorTranslator.FromHtml($"#{font_color}").A, ColorTranslator.FromHtml($"#{font_color}").R, ColorTranslator.FromHtml($"#{font_color}").G, ColorTranslator.FromHtml($"#{font_color}").B);
                }


                ws_trg.Cells[trg_row, trg_col].Style.Font.Bold = bold_cell;
                ws_trg.Cells[trg_row, trg_col].Style.Font.Name = fontname_cell;
                ws_trg.Cells[trg_row, trg_col].Style.Font.Size = size_cell;
                ws_trg.Cells[trg_row, trg_col].Style.Font.UnderLine = underline_cell;
                ws_trg.Cells[trg_row, trg_col].Style.Font.Italic = italic_cell;
                ws_trg.Cells[trg_row, trg_col].Style.Font.Strike = strikethrough_cell;

                if (fill_bkcolor != null && fill_bkcolor != "")
                {
                    ws_trg.Cells[trg_row, trg_col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws_trg.Cells[trg_row, trg_col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml($"#{fill_bkcolor}").A, ColorTranslator.FromHtml($"#{fill_bkcolor}").R, ColorTranslator.FromHtml($"#{fill_bkcolor}").G, ColorTranslator.FromHtml($"#{fill_bkcolor}").B);
                }





            }

            if (target_has_color == true)
            {
                if (target_bkcolor != "")
                {
                    ws_trg.Cells[trg_row, trg_col].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    ws_trg.Cells[trg_row, trg_col].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml($"#{target_bkcolor}").A, ColorTranslator.FromHtml($"#{target_bkcolor}").R, ColorTranslator.FromHtml($"#{target_bkcolor}").G, ColorTranslator.FromHtml($"#{target_bkcolor}").B);
                }

            }


            /*wip*/
        }

        private void XlsxToXls2003(string worksheetname_src, string worksheetname_trg, int src_row, int src_col, int trg_row, int trg_col)
        {

            //offset correction
            trg_row = trg_row - 1;
            trg_col = trg_col - 1;

            ExcelWorksheet excel_ws = epplus_excel_src.Workbook.Worksheets[worksheetname_src];

            HSSFSheet original_ws = (HSSFSheet)hssf_excel.GetSheetAt(hssf_excel.GetSheetIndex(worksheetname_trg));

            //HSSF Stuff happens here
            //select the row
            HSSFRow hssfRow = (HSSFRow)original_ws.GetRow(trg_row);

            if (hssfRow == null)
            {
                hssfRow = (HSSFRow)original_ws.CreateRow(trg_row);
            }

            HSSFCell hssfCell = (HSSFCell)hssfRow.GetCell(trg_col);

            if (hssfCell == null)
            {
                hssfCell = (HSSFCell)hssfRow.CreateCell(trg_col);
            }

            string font_color = excel_ws.Cells[src_row, src_col].Style.Font.Color.Rgb;
            AddColor(font_color);

            string fill_bkcolor = excel_ws.Cells[src_row, src_col].Style.Fill.BackgroundColor.Rgb;
            AddColor(fill_bkcolor);

            string pattern_color = excel_ws.Cells[src_row, src_col].Style.Fill.PatternColor.Rgb;
            AddColor(pattern_color);

            //added 2020-12-18 -- check if the target cell has color already and don't over write it

            bool hssfHasColorAlready = false;

            if (hssfCell.CellStyle.FillPattern != FillPattern.NoFill)
            {
                hssfHasColorAlready = true;
                Console.WriteLine($"{trg_row}::{trg_col}::pattern={hssfCell.CellStyle.FillPattern}\n{hssfCell.CellStyle.FillBackgroundColor}");
            }


            //intrinsic font, size, bold, underline, italic, strikethrough 
            string fontname_cell = excel_ws.Cells[src_row, src_col].Style.Font.Name;
            float size_cell = excel_ws.Cells[src_row, src_col].Style.Font.Size;
            bool bold_cell = excel_ws.Cells[src_row, src_col].Style.Font.Bold;
            bool underline_cell = excel_ws.Cells[src_row, src_col].Style.Font.UnderLine;
            bool italic_cell = excel_ws.Cells[src_row, src_col].Style.Font.Italic;
            bool strikethrough_cell = excel_ws.Cells[src_row, src_col].Style.Font.Strike;


            /////
            HSSFCellStyle hSSFCellPatternStyle = (HSSFCellStyle)hssf_excel.CreateCellStyle();
            HSSFPalette hffsPalettePattern = hssf_excel.GetCustomPalette();

            //fetch the color here
            int hssfcolorIndexPatten = -1; //-1 for auto color

            //Console.WriteLine($"My Pattern color is {pattern_color} and Fill background color {fill_bkcolor}");
            if (fill_bkcolor == null)
            {
                fill_bkcolor = "0";
            }
            foreach (ColorIndex cix in colorLists.Where(x => x.GetColor == fill_bkcolor.ToLower()))
            {
                if (cix.GetColor != "Auto")
                {

                    Color epplusColor = ColorTranslator.FromHtml($"#{fill_bkcolor}");
                    hffsPalettePattern.SetColorAtIndex((short)cix.GetIndex, epplusColor.R, epplusColor.G, epplusColor.B);
                    hssfcolorIndexPatten = cix.GetIndex;

                }

            }
            //Console.WriteLine($"color pattern index found = {hssfcolorIndexPatten}");



            if (hssfHasColorAlready == true)
            {
                hSSFCellPatternStyle.FillPattern = FillPattern.SolidForeground;
                hSSFCellPatternStyle.FillForegroundColor = hssfCell.CellStyle.FillForegroundColor;
            }
            else
            {
                //assign the color here
                if (hssfcolorIndexPatten > -1)
                {
                    hSSFCellPatternStyle.FillPattern = FillPattern.SolidForeground;
                    hSSFCellPatternStyle.FillForegroundColor = IndexedColors.ValueOf(hssfcolorIndexPatten).Index;

                }
                else
                {

                    hSSFCellPatternStyle.FillPattern = FillPattern.NoFill;
                    hSSFCellPatternStyle.FillForegroundColor = IndexedColors.Automatic.Index;


                }
            }

            hssfCell.CellStyle = hSSFCellPatternStyle;



            string textvalue = "";
            if (excel_ws.Cells[src_row, src_col].Value != null)
            {
                textvalue = excel_ws.Cells[src_row, src_col].Value.ToString();
                textvalue = textvalue.Replace("\t", " ").Replace("\r", "").TrimEnd();
            }


            int richTextCount = excel_ws.Cells[src_row, src_col].RichText.Count;
            //Console.WriteLine($"\nrich text number = {richTextCount}\nHas richText = {excel_ws.Cells[src_row, src_col].IsRichText}");

            if (excel_ws.Cells[src_row, src_col].IsRichText == true)
            {
                //Console.WriteLine("Rich Text src_color found");

                HSSFRichTextString hssfRichTextString = new HSSFRichTextString(textvalue);
                hssfCell.SetCellValue(hssfRichTextString);

                int richTextElementIndex = 0;

                for (int i = 0; i < richTextCount; i++)
                {

                    //text properties from epplus
                    Color epplusColor = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).Color;
                    bool epplusBold = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).Bold;
                    string epplusFontname = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).FontName;
                    float epplusSize = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).Size;
                    bool epplusUnderline = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).UnderLine;
                    bool epplusItalic = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).Italic;
                    bool epplusStrike = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).Strike;
                    ExcelVerticalAlignmentFont epplusValign = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).VerticalAlign;
                    //bool epplusPspace = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).PreserveSpace;

                    string epplusText = excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).Text;

                    //Console.WriteLine($"text = { excel_ws.Cells[src_row, src_col].RichText.ElementAt(i).Text}");
                    //Console.WriteLine($"text color = {epplusColor.Name.ToString()}");
                    AddColor(epplusColor.Name);


                    //assign the font attributes to hssf object

                    //color
                    HSSFFont hssfFont = (HSSFFont)hssf_excel.CreateFont();
                    HSSFPalette hffsPalette = hssf_excel.GetCustomPalette();
                    hssfFont.IsBold = epplusBold;
                    hssfFont.FontName = epplusFontname;
                    hssfFont.FontHeightInPoints = (double)epplusSize;

                    switch (epplusValign)
                    {
                        case ExcelVerticalAlignmentFont.None:
                            hssfFont.TypeOffset = FontSuperScript.None;
                            break;
                        case ExcelVerticalAlignmentFont.Baseline:
                            hssfFont.TypeOffset = FontSuperScript.None;
                            break;
                        case ExcelVerticalAlignmentFont.Subscript:
                            hssfFont.TypeOffset = FontSuperScript.Sub;
                            break;
                        case ExcelVerticalAlignmentFont.Superscript:
                            hssfFont.TypeOffset = FontSuperScript.Super;
                            break;
                        default:
                            break;
                    }


                    if (epplusUnderline == true)
                    {
                        hssfFont.Underline = FontUnderlineType.Single;
                    }

                    hssfFont.IsItalic = epplusItalic;
                    hssfFont.IsStrikeout = epplusStrike;


                    //fetch the color here
                    int hssfcolorIndex = -1; //-1 for auto color
                    foreach (ColorIndex cix in colorLists.Where(x => x.GetColor == epplusColor.Name))
                    {
                        if (cix.GetColor != "Auto")
                        {
                            hffsPalette.SetColorAtIndex((short)cix.GetIndex, epplusColor.R, epplusColor.G, epplusColor.B);
                            hssfcolorIndex = cix.GetIndex;
                        }

                    }
                    Console.WriteLine($"color index found = {hssfcolorIndex}");

                    //assign the color here
                    if (hssfcolorIndex > -1)
                    {
                        hssfFont.Color = IndexedColors.ValueOf(hssfcolorIndex).Index;
                    }
                    else
                    {
                        hssfFont.Color = IndexedColors.Automatic.Index;
                    }


                    //finally apply everything

                    //Console.WriteLine($"text = {epplusText}\nlenght = {epplusText.Length} :: total lenght = {excel_ws.Cells[src_row, src_col].Value.ToString().Length}");
                    //Console.WriteLine($"start = {richTextElementIndex} end = {richTextElementIndex + epplusText.Length} hssfont = {hssfFont}");

                    hssfRichTextString.ApplyFont(richTextElementIndex, richTextElementIndex + epplusText.Length, hssfFont);

                    richTextElementIndex = richTextElementIndex + epplusText.Length;


                }

            }
            else
            {

                //Console.WriteLine($"FOO BAR = {textvalue}");
                hssfCell.SetCellValue(textvalue);

                //next we need to set the colors for this cell (wip)
                //assign the font attributes to hssf object
                HSSFFont hssfFont = (HSSFFont)hssf_excel.CreateFont();

                HSSFCellStyle hSSFCellStyle = (HSSFCellStyle)hssf_excel.CreateCellStyle();
                HSSFPalette hffsPalette = hssf_excel.GetCustomPalette();


                //fetch the color here
                int hssfcolorIndex = -1; //-1 for auto color

                //Console.WriteLine($"My font color is {font_color}");
                if (font_color == null)
                {
                    font_color = "0";
                }
                foreach (ColorIndex cix in colorLists.Where(x => x.GetColor == font_color.ToLower()))
                {
                    if (cix.GetColor != "Auto")
                    {


                        Color epplusColor = ColorTranslator.FromHtml($"#{font_color}");
                        hffsPalette.SetColorAtIndex((short)cix.GetIndex, epplusColor.R, epplusColor.G, epplusColor.B);
                        hssfcolorIndex = cix.GetIndex;

                    }

                }
                //Console.WriteLine($"color index found = {hssfcolorIndex} font ={hssfFont}");

                //assign the color here
                if (hssfcolorIndex > -1)
                {
                    hssfFont.Color = IndexedColors.ValueOf(hssfcolorIndex).Index;
                }
                else
                {
                    hssfFont.Color = IndexedColors.Automatic.Index;
                }



                hssfFont.IsBold = bold_cell;
                hssfFont.FontName = fontname_cell;
                hssfFont.FontHeightInPoints = (double)size_cell;

                if (underline_cell == true)
                {
                    hssfFont.Underline = FontUnderlineType.Single;
                }

                hssfFont.IsItalic = italic_cell;
                hssfFont.IsStrikeout = strikethrough_cell;

                hSSFCellStyle.SetFont(hssfFont);


                hSSFCellStyle.FillForegroundColor = hSSFCellPatternStyle.FillForegroundColor;
                hSSFCellStyle.FillPattern = hSSFCellPatternStyle.FillPattern;


                hssfCell.CellStyle = hSSFCellStyle;


            }


        }

        private void Xls2003ToXlsx(string worksheetname_src, string worksheetname_trg, int src_row, int src_col, int trg_row, int trg_col)
        {
            //trg_col= trg_col - 1;

            HSSFSheet src_ws = (HSSFSheet)hssf_excel.GetSheetAt(hssf_excel.GetSheetIndex(worksheetname_src));
            ExcelWorksheet ws_trg = epplus_excel_trg.Workbook.Worksheets[worksheetname_trg];
            Console.WriteLine($"src = {worksheetname_src} trg = {worksheetname_trg} src_row = {src_row} src_col = {src_col} trg_row = {trg_row} trg_col = {trg_col}");

            HSSFRow rowxls = (HSSFRow)src_ws.GetRow(src_row);
            if (rowxls.GetCell(src_col) != null)
            {
                HSSFCell hssfcell = (HSSFCell)rowxls.GetCell(src_col);

                //change this up but for now we just need to confirm that this works
                //string segment = string.Empty;

                try
                {


                    HSSFRichTextString contents = (HSSFRichTextString)hssfcell.RichStringCellValue;

                    Console.WriteLine($"formatting runs = {contents.NumFormattingRuns}");

                    //Reading RichText from an XLS file
                    if (contents.NumFormattingRuns > 0)
                    {
                        ws_trg.Cells[trg_row, trg_col].Value = null;
                        ws_trg.Cells[trg_row, trg_col].RichText.Clear();
                        ws_trg.Cells[trg_row, trg_col].IsRichText = true;

                        for (int k = 0; k < contents.NumFormattingRuns; k++)
                        {
                            int begin = contents.GetIndexOfFormattingRun(k);
                            short fontIndex = contents.GetFontOfFormattingRun(k);

                            int lentgh = 0;
                            for (int j = begin; j < contents.Length; j++)
                            {
                                short currFontIndex = contents.GetFontAtIndex(j);
                                if (currFontIndex == fontIndex)
                                {
                                    lentgh++;
                                }
                                else
                                {
                                    break;
                                }
                            }


                            HSSFFont xlsfont = (HSSFFont)hssf_excel.GetFontAt(fontIndex);

                            //more thought needs to be give to this since it has palletized colors and this infomation is stored
                            short xlscolor = xlsfont.Color;
                            byte[] hssfcolor = hssf_excel.GetCustomPalette().GetColor(xlscolor).RGB;

                            bool xlsbold = xlsfont.IsBold;
                            bool xlsitalic = xlsfont.IsItalic;
                            bool xlsstrike = xlsfont.IsStrikeout;
                            double xlsfontheight = xlsfont.FontHeight;
                            string xlsfontname = xlsfont.FontName;
                            double xlsfontheightpoints = xlsfont.FontHeightInPoints;
                            FontSuperScript xlsfonttypeoffset = xlsfont.TypeOffset;
                            FontUnderlineType xlsunderline = xlsfont.Underline;

                            Color xlscolorColor = Color.FromArgb(
                                    hssfcolor[0],
                                    hssfcolor[1],
                                    hssfcolor[2]
                                );

                            string xls2003color = ColorTranslator.ToHtml(
                                Color.FromArgb(
                                    hssfcolor[0],
                                    hssfcolor[1],
                                    hssfcolor[2]
                                ));

                            string xlstext = contents.ToString().Substring(begin, lentgh);

                            Console.WriteLine($"word = {xlstext}" +
                                $"bold = {xlsbold}\n" +
                                $"italic = {xlsitalic}\n" +
                                $"strike = {xlsstrike}\n" +
                                $"fontheight = {xlsfontheight}\n" +
                                $"fontheight (points) = {xlsfontheightpoints}\n" +
                                $"TypeOffset = {xlsfonttypeoffset}\n" +
                                $"Underlined = {xlsunderline}\n" +
                                $"Color = {xlscolor}\n" +
                                $"hssfcolor = {hssfcolor[0]},{hssfcolor[1]},{hssfcolor[2]}\n" +
                                $"xls2003color = {xls2003color}\n\n");


                            ws_trg.Cells[trg_row, trg_col].RichText.Insert(k, xlstext);
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).Color = xlscolorColor;
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).Bold = xlsbold;
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).FontName = xlsfontname;
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).Size = (float)xlsfontheightpoints;

                            switch (xlsunderline)
                            {
                                case FontUnderlineType.Single:
                                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).UnderLine = true;
                                    break;
                                case FontUnderlineType.None:
                                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).UnderLine = false;
                                    break;
                                default:
                                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).UnderLine = false;
                                    break;
                            }

                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).Italic = xlsitalic;
                            ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).Strike = xlsstrike;

                            switch (xlsfonttypeoffset)
                            {
                                case FontSuperScript.None:
                                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).VerticalAlign = ExcelVerticalAlignmentFont.None;
                                    break;
                                case FontSuperScript.Sub:
                                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).VerticalAlign = ExcelVerticalAlignmentFont.Subscript;
                                    break;
                                case FontSuperScript.Super:
                                    ws_trg.Cells[trg_row, trg_col].RichText.ElementAt(k).VerticalAlign = ExcelVerticalAlignmentFont.Superscript;
                                    break;
                                default:
                                    break;

                            }

                        }


                    }
                    else
                    {
                        //Console.WriteLine($"foo contents = {contents}");


                        bool autocolor = false;

                        //we will apply just the intrnsic colors to the cell
                        //segment = hssfcell.RichStringCellValue.ToString();
                        HSSFCellStyle cs = (HSSFCellStyle)hssfcell.CellStyle;

                        HSSFFont xlsfont = (HSSFFont)cs.GetFont(hssf_excel);

                        //more thought needs to be give to this since it has palletized colors and this infomation is stored
                        short xlscolor = xlsfont.Color;

                        Console.WriteLine(xlscolor);

                        if(xlscolor >= 32767)
                        {
                            xlscolor = 8; //this is an automatic color
                            autocolor = true;
                        }
                        byte[] hssfcolor = hssf_excel.GetCustomPalette().GetColor(xlscolor).RGB;

                        bool xlsbold = xlsfont.IsBold;
                        bool xlsitalic = xlsfont.IsItalic;
                        bool xlsstrike = xlsfont.IsStrikeout;
                        double xlsfontheight = xlsfont.FontHeight;
                        string xlsfontname = xlsfont.FontName;
                        double xlsfontheightpoints = xlsfont.FontHeightInPoints;
                        FontSuperScript xlsfonttypeoffset = xlsfont.TypeOffset;
                        FontUnderlineType xlsunderline = xlsfont.Underline;

                        Color xlscolorColor = Color.FromArgb(
                                hssfcolor[0],
                                hssfcolor[1],
                                hssfcolor[2]
                            );


                        //string xlstext = contents.ToString().Substring(begin, lentgh);
                        Console.WriteLine($"word = {contents.ToString()}" +
                            $"bold = {xlsbold}\n" +
                            $"italic = {xlsitalic}\n" +
                            $"strike = {xlsstrike}\n" +
                            $"fontheight = {xlsfontheight}\n" +
                            $"fontheight (points) = {xlsfontheightpoints}\n" +
                            $"TypeOffset = {xlsfonttypeoffset}\n" +
                            $"Underlined = {xlsunderline}\n" +
                            $"Color = {xlscolor}\n" +
                            $"hssfcolor = {hssfcolor[0]},{hssfcolor[1]},{hssfcolor[2]}\n\n");

                        ws_trg.Cells[trg_row, trg_col].Style.Font.Name = xlsfontname;
                        ws_trg.Cells[trg_row, trg_col].Style.Font.Bold = xlsbold;
                        ws_trg.Cells[trg_row, trg_col].Style.Font.Italic = xlsitalic;
                        ws_trg.Cells[trg_row, trg_col].Style.Font.Strike = xlsstrike;
                        ws_trg.Cells[trg_row, trg_col].Style.Font.Size = (float)xlsfontheightpoints;

                        if(autocolor == false)
                        {
                            ws_trg.Cells[trg_row, trg_col].Style.Font.Color.SetColor(xlscolorColor);
                        }
                        

                        switch (xlsunderline)
                        {
                            case FontUnderlineType.Single:
                                ws_trg.Cells[trg_row, trg_col].Style.Font.UnderLine = true;
                                break;
                            case FontUnderlineType.None:
                                ws_trg.Cells[trg_row, trg_col].Style.Font.UnderLine = false;
                                break;
                            default:
                                ws_trg.Cells[trg_row, trg_col].Style.Font.UnderLine = false;
                                break;
                        }

                        switch (xlsfonttypeoffset)
                        {
                            case FontSuperScript.None:
                                ws_trg.Cells[trg_row, trg_col].Style.Font.VerticalAlign = ExcelVerticalAlignmentFont.None;
                                break;
                            case FontSuperScript.Sub:
                                ws_trg.Cells[trg_row, trg_col].Style.Font.VerticalAlign = ExcelVerticalAlignmentFont.Subscript;
                                break;
                            case FontSuperScript.Super:
                                ws_trg.Cells[trg_row, trg_col].Style.Font.VerticalAlign = ExcelVerticalAlignmentFont.Superscript;
                                break;
                            default:
                                break;

                        }


                        ws_trg.Cells[trg_row, trg_col].Value = contents;



                    }



                }
                catch
                {
                    //segment = hssfcell.NumericCellValue.ToString();
                }

                //excel_ws.Cells[trg_row, trg_col].Value = segment;
            }


        }

        public void AddColor(string color)
        {
            //colorLists.Add(new ColorIndex(colorCount, color));
            //colorCount++;


            //Console.WriteLine("ADD COLOR FUNCTION");
            if (color == null || color == "" || color == "0")
            {
                color = "auto";
            }
            color = color.ToLower();


            ColorIndex color_exists = colorLists.Where(x => x.GetColor == color).FirstOrDefault();


            int index_offset = 9;
            if (color_exists == null)
            {

                if (colorLists.Count() + index_offset < 64)
                {
                    //Console.WriteLine($"Assiging Index = {colorLists.Count() + index_offset} to color {color} and adding to color list");
                    colorLists.Add(new ColorIndex(colorLists.Count() + index_offset, color));
                }
                else
                {
                    //Console.WriteLine("Maximum number of color reached");
                }
            }
            else
            {

                //Console.WriteLine($"This color {color_exists.GetColor} at Index {color_exists.GetIndex} exists already");
            }




        }

    }

    public class ColorIndex
    {
        private int index;
        private string color;

        public ColorIndex(int index, string color)
        {
            this.index = index;
            this.color = color;
        }

        public int GetIndex
        {
            get { return index; }
        }

        public string GetColor
        {
            get { return color; }
        }

    }


    /* ----- End of new Onion code ----*/


}

