﻿using NPOI.HSSF.UserModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using static Mono_tabby.ExcelUtils;

namespace Mono_tabby
{
    class Program
    {

        static void Main(string[] args)
        {



            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            bool option = false;
            while (option == false)
            {
                Console.WriteLine("Choose an option:\n0 - Make memoQ friendly files for import\n1 - Restore translations\n2 - Fix internal Jankiness in files exported from memoQ\n3 - Fix illegal characters");
                string choice = Console.ReadLine();

                if (choice == "0")
                {
                    CreateImportFiles();
                    //option = true;
                }

                if (choice == "1")
                {
                    RestoreOriginalFiles();
                    //option = true;
                }

                if (choice == "2")
                {
                    FixJankyFiles();
                    //option = true;
                }

                if (choice == "3")
                {
                    FixIllegalChars();
                }


            }


            Console.WriteLine("Done!");
            Console.ReadLine();
        }

        static string GetLanguageFile()
        {
            string DefaultPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "MonoTabby");
            string[] languagefiles = Directory.GetFiles(DefaultPath, "*.xlsx", SearchOption.TopDirectoryOnly);


            while (true)
            {
                Console.WriteLine("Choose the language file");
                for (int i = 0; i < languagefiles.Count(); i++)
                {
                    Console.WriteLine($"{i} - {new FileInfo(languagefiles[i]).Name}");
                }

                Console.WriteLine($"Enter a value from 0 - {languagefiles.Count() - 1}");
                int choice = Convert.ToInt32(Console.ReadLine());

                if (File.Exists(languagefiles[choice]))
                {
                    return languagefiles[choice];
                }

            }



        }


        static void CreateImportFiles()
        {

            string language_keys = GetLanguageFile();
            /*
            if (!File.Exists(language_keys))
            {
                //language keys
                Console.WriteLine("Copy location to the languages.xlsx file");
                language_keys = Console.ReadLine();
                @language_keys = language_keys.Replace("\"", "");
            }
            */

            //check if this is a Directory or file
            Console.WriteLine("Copy path to folder containing files to import -=OR=- Copy and paste the individual file");
            string @path_file = Console.ReadLine();

            path_file = path_file.Replace("\"", "");


            bool option = false;
            bool keep_formatting = false;
            while (option == false)
            {
                Console.WriteLine("Do you want to keep formatting? y/n");
                string keep_format = Console.ReadLine();

                if (keep_format.ToLower() == "y")
                {
                    keep_formatting = true;
                    option = true;
                }
                if (keep_format.ToLower() == "n")
                {
                    keep_formatting = false;
                    option = true;
                }
            }


            if (path_file.ToLower().EndsWith(".xlsx") || path_file.ToLower().EndsWith(".xls") || path_file.ToLower().EndsWith(".xlsm"))
            {
                Console.WriteLine("Processing this is file");

                if (path_file.ToLower().EndsWith(".xls"))
                {
                    FileToImportXLS2003(path_file, language_keys, keep_formatting);
                }
                else
                {
                    FileToImportXLSXM(path_file, language_keys, keep_formatting);
                }

            }
            else
            {
                Console.WriteLine("Listing files");
                string[] files = Directory.GetFiles(path_file, "*.xls*");

                foreach (string file in files)
                {
                    Console.WriteLine(file);
                    if (file.ToLower().EndsWith(".xls"))
                    {
                        FileToImportXLS2003(file, language_keys, keep_formatting);
                    }
                    else
                    {
                        FileToImportXLSXM(file, language_keys, keep_formatting);
                    }

                }

            }
        }

        static void FileToImportXLSXM(string src_file, string language_keys, bool keep_formatting)
        {

            ExcelPackage target_excel = new ExcelPackage();
            ExcelWorksheet trg_ws = target_excel.Workbook.Worksheets.Add("Sheet1");


            List<Languages> header = new List<Languages>();
            ExcelPackage language_file = new ExcelPackage(new FileInfo(language_keys));
            ExcelWorksheet languages = language_file.Workbook.Worksheets[0];

            ExcelPackage source_excel = new ExcelPackage(new FileInfo(src_file));
            //ExcelPackage target_excel = new ExcelPackage();
            //ExcelWorksheet trg_ws = target_excel.Workbook.Worksheets.Add("Sheet1");

            //Richtext
            RichTextCopy richTextCopy = new RichTextCopy(source_excel, target_excel);

            //Get our headers and their varients
            for (int col = 1; col < languages.Dimension.Columns + 1; col++)
            {
                if (languages.Cells[1, col].Value != null)
                {
                    string head = languages.Cells[1, col].Value.ToString();
                    string colindex = languages.Cells[2, col].Value.ToString();

                    Console.WriteLine($"{head} -- {colindex}");

                    for (int row = 3; row < languages.Dimension.Rows + 1; row++)
                    {

                        if (languages.Cells[row, col].Value != null)
                        {
                            header.Add(new Languages(head, colindex, languages.Cells[row, col].Value.ToString()));
                        }

                    }
                }

            }

            //create our blank workbook
            //filename, worksheet, row, index_1, varient_1, index_n, varient_n, comments_1, comments_n, ....

            int trg_row = 1;
            trg_ws.Cells[trg_row, 1].Value = "Filename";
            trg_ws.Cells[trg_row, 2].Value = "Worksheet";
            trg_ws.Cells[trg_row, 3].Value = "Row";
            trg_row++;

            int key = 4;
            int coln = 4;
            int colnCount = 1;
            string current_header = string.Empty;

            List<HeaderKeys> local_header = new List<HeaderKeys>();

            foreach (Languages varients in header)
            {

                if (current_header != varients.Header)
                {
                    current_header = varients.Header;
                    Console.WriteLine($"{current_header} - Index this column: {varients.IndexCol}");

                    if (varients.IndexCol.ToLower() == "y" || varients.IndexCol.ToLower() == "ask")
                    {
                        trg_ws.Cells[1, coln].Value = $"Index_{colnCount}_{current_header.ToUpper()}";
                        //local_header.Add($"Index_{colnCount}_{current_header.ToUpper()}");
                        local_header.Add(new HeaderKeys(key, $"Index_{colnCount}_{current_header.ToUpper()}"));
                        coln++;
                        colnCount++;
                        key++;

                    }

                    trg_ws.Cells[1, coln].Value = current_header.ToUpper();
                    local_header.Add(new HeaderKeys(key, $"{current_header.ToUpper()}"));

                    coln++;
                    key++;

                }

            }


            foreach (HeaderKeys lang in local_header)
            {
                Console.WriteLine($"{lang.Index} -- {lang.Lang}");
            }


            foreach (ExcelWorksheet src_ws in source_excel.Workbook.Worksheets)
            {
                //find header -- if we don't find it in the first 10 rows
                int found = 0;
                int rown = 1;


                List<HeaderMap> headermap = new List<HeaderMap>();
                if (src_ws.Hidden.ToString() == "Visible")
                {
                    while (found == 0 && rown <= 10)
                    {
                        for (int col = 1; col < src_ws.Dimension.Columns + 1; col++)
                        {
                            if (src_ws.Cells[rown, col].Value != null)
                            {
                                string sentence = src_ws.Cells[rown, col].Value.ToString().ToLower();

                                foreach (Languages varients in header.Where(x => x.Varient.ToLower() == sentence))
                                {
                                    if (varients.IndexCol == "y" || varients.IndexCol == "ask")
                                    {
                                        foreach (HeaderKeys mapped_header in local_header.Where(x => x.Lang.ToLower() == varients.Header))
                                        {
                                            headermap.Add(new HeaderMap(mapped_header.Index, col, varients.Header, true));
                                        }
                                        found++;
                                    }
                                    else
                                    {
                                        foreach (HeaderKeys mapped_header in local_header.Where(x => x.Lang.ToLower() == varients.Header))
                                        {
                                            headermap.Add(new HeaderMap(mapped_header.Index, col, varients.Header, false));
                                        }
                                    }
                                }
                            }
                        }

                        rown++;

                    }
                    if (found > 0)
                    {
                        Console.WriteLine($"Found header on row {rown - 1} Processing sheet {src_ws.Name}");

                        foreach (HeaderMap item in headermap)
                        {
                            Console.WriteLine($"local header column = {item.Index} language {item.Lang} col with text {item.Col} index exists? {item.Indexed}");
                        }

                        for (int row = rown; row < src_ws.Dimension.Rows + 1; row++)
                        {
                            if (src_ws.Row(row).Hidden == false)
                            {
                                int content = 0;

                                //exclude by color

                                //color exclusion
                                bool exclude_row = false;



                                foreach (HeaderMap item in headermap)
                                {
                                    if (item.Lang.ToLower().StartsWith("comment") == false)
                                    {
                                        if (src_ws.Cells[row, item.Col].Value != null)
                                        {
                                            //exclude colors

                                            if (src_ws.Cells[row, item.Col].Style.Fill.BackgroundColor.LookupColor() == "#FF808080" ||
                                                src_ws.Cells[row, item.Col].Style.Fill.BackgroundColor.LookupColor() == "#FFA6A6A6" ||
                                                src_ws.Cells[row, item.Col].Style.Fill.BackgroundColor.LookupColor() == "#FFBFBFBF" ||
                                                src_ws.Cells[row, item.Col].Style.Fill.BackgroundColor.LookupColor() == "#FFD9D9D9")
                                            {
                                                //uncomment for SNK
                                                //exclude_row = true;

                                            }

                                            if (src_ws.Cells[row, item.Col].Value.ToString() != "")
                                            {
                                                content++;
                                            }
                                        }
                                    }
                                }


                                if (content > 0)
                                {
                                    if (exclude_row == false)
                                    {
                                        trg_ws.Cells[trg_row, 1].Value = new FileInfo(src_file).Name;
                                        trg_ws.Cells[trg_row, 2].Value = src_ws.Name;
                                        trg_ws.Cells[trg_row, 3].Value = row;

                                        foreach (HeaderMap item in headermap)
                                        {
                                            if (src_ws.Cells[row, item.Col].Value != null)
                                            {
                                                string segment = src_ws.Cells[row, item.Col].Value.ToString();
                                                segment = segment.Replace("\t", " ").Replace("\r", "");

                                                //write this to the cell without formatting
                                                if (keep_formatting == false)
                                                {
                                                    trg_ws.Cells[trg_row, item.Index].Value = segment.TrimEnd();
                                                    //Console.WriteLine($"{segment.TrimEnd()}");
                                                }
                                                else
                                                {
                                                    richTextCopy.CopyCell(src_ws.Name, "Sheet1", row, item.Col, trg_row, item.Index);
                                                }


                                            }
                                            if (item.Indexed == true)
                                            {
                                                trg_ws.Cells[trg_row, item.Index - 1].Value = ExcelUtils.ColumnIndexToColumnLetter(item.Col);
                                            }
                                        }

                                        trg_row++;
                                    }
                                    else
                                    {
                                        Console.WriteLine($"Excluding Row: {row} [Ignore color]");
                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Skipping sheet {src_ws.Name}...");
                    }
                }
            }



            if (src_file.ToLower().EndsWith(".xlsx"))
            {
                target_excel.SaveAs(new FileInfo(src_file.Replace(".xlsx", "_Mono_Tab_Prepped.xlsx")));
            }

            if (src_file.ToLower().EndsWith(".xlsm"))
            {
                target_excel.SaveAs(new FileInfo(src_file.Replace(".xlsm", "_Mono_Tab_Prepped.xlsx")));
            }


            if (keep_formatting == true)
            {
                Console.WriteLine("Finalizing colors in file");
                string current_name = string.Empty;
                if (src_file.ToLower().EndsWith(".xlsx"))
                    current_name = src_file.Replace(".xlsx", "_Mono_Tab_Prepped.xlsx");

                if (src_file.ToLower().EndsWith(".xlsm"))
                    current_name = src_file.Replace(".xlsm", "_Mono_Tab_Prepped.xlsx");

                string updated_name = current_name.Replace("_Mono_Tab_Prepped.xlsx", "_Mono_Tab_Prepped_Keep_Formatting.xlsx");
                JankyFileToFix(current_name);
                File.Delete(current_name);
                //File.Move(updated_name, current_name);
            }


        }


        static void FileToImportXLS2003(string src_file, string language_keys, bool keep_formatting)
        {

            ExcelPackage target_excel = new ExcelPackage();
            ExcelWorksheet trg_ws = target_excel.Workbook.Worksheets.Add("Sheet1");


            //special case where we need to convert the file
            Console.WriteLine($"Converting ... {src_file} to xlsx");

            /* WIP use direct conversion */

            List<Languages> header = new List<Languages>();
            ExcelPackage language_file = new ExcelPackage(new FileInfo(language_keys));
            ExcelWorksheet languages = language_file.Workbook.Worksheets[0];

            //ExcelPackage source_excel = new ExcelPackage(new FileInfo(src_file));
            //NPOI reads the file here
            HSSFWorkbook source_excel;

            using (FileStream xls_file = new FileStream(src_file, FileMode.Open, FileAccess.Read))
            {
                source_excel = new HSSFWorkbook(xls_file);
            }

            //ExcelPackage target_excel = new ExcelPackage();
            //ExcelWorksheet trg_ws = target_excel.Workbook.Worksheets.Add("Sheet1");

            //Richtext
            RichTextCopy richTextCopy = new RichTextCopy(source_excel, target_excel);

            //Get our headers and their varients
            for (int col = 1; col < languages.Dimension.Columns + 1; col++)
            {
                if (languages.Cells[1, col].Value != null)
                {
                    string head = languages.Cells[1, col].Value.ToString();
                    string colindex = languages.Cells[2, col].Value.ToString();

                    Console.WriteLine($"{head} -- {colindex}");

                    for (int row = 3; row < languages.Dimension.Rows + 1; row++)
                    {

                        if (languages.Cells[row, col].Value != null)
                        {
                            header.Add(new Languages(head, colindex, languages.Cells[row, col].Value.ToString()));
                        }

                    }
                }

            }

            //create our blank workbook
            //filename, worksheet, row, index_1, varient_1, index_n, varient_n, comments_1, comments_n, ....

            int trg_row = 1;
            trg_ws.Cells[trg_row, 1].Value = "Filename";
            trg_ws.Cells[trg_row, 2].Value = "Worksheet";
            trg_ws.Cells[trg_row, 3].Value = "Row";
            trg_row++;

            int key = 4;
            int coln = 4;
            int colnCount = 1;
            string current_header = string.Empty;

            List<HeaderKeys> local_header = new List<HeaderKeys>();

            foreach (Languages varients in header)
            {

                if (current_header != varients.Header)
                {
                    current_header = varients.Header;
                    Console.WriteLine($"{current_header} - Index this column: {varients.IndexCol}");

                    if (varients.IndexCol.ToLower() == "y" || varients.IndexCol.ToLower() == "ask")
                    {
                        trg_ws.Cells[1, coln].Value = $"Index_{colnCount}_{current_header.ToUpper()}";
                        //local_header.Add($"Index_{colnCount}_{current_header.ToUpper()}");
                        local_header.Add(new HeaderKeys(key, $"Index_{colnCount}_{current_header.ToUpper()}"));
                        coln++;
                        colnCount++;
                        key++;

                    }

                    trg_ws.Cells[1, coln].Value = current_header.ToUpper();
                    local_header.Add(new HeaderKeys(key, $"{current_header.ToUpper()}"));

                    coln++;
                    key++;

                }

            }


            foreach (HeaderKeys lang in local_header)
            {
                Console.WriteLine($"{lang.Index} -- {lang.Lang}");
            }

            List<HeaderMap> headermap = new List<HeaderMap>();
            //process the sheets

            Console.WriteLine($"{source_excel.NumberOfSheets}");
            for (int ws = 0; ws < source_excel.NumberOfSheets; ws++)
            {
                HSSFSheet src_ws = (HSSFSheet)source_excel.GetSheetAt(ws);
                HSSFRow hssfRow = (HSSFRow)src_ws.GetRow(0);


                int found = 0;
                int rown = 0;

                string worksheet = src_ws.SheetName;
                int lastrow = src_ws.LastRowNum;
                int lastcell = hssfRow.LastCellNum;
                if (source_excel.IsSheetHidden(ws) == false)
                {
                    Console.WriteLine("Visible");
                    Console.WriteLine($"" +
                    $"Sheet Name: {src_ws.SheetName}\n" +
                    $"Last Row: {src_ws.LastRowNum}\n" +
                    $"Last Col: {hssfRow.LastCellNum}");

                    //find our headers in the sheet within the first 10 rows

                    while (found == 0 && rown <= 10)
                    {
                        for (int col = 0; col < lastcell; col++)
                        {
                            //we put the breaks on so we don't exceed the number or rows
                            if (rown <= lastrow)
                            {
                                if ((HSSFRow)src_ws.GetRow(rown) != null)
                                {
                                    HSSFRow rowxls = (HSSFRow)src_ws.GetRow(rown);
                                    if (rowxls.GetCell(col) != null)
                                    {
                                        HSSFCell hssfcell = (HSSFCell)rowxls.GetCell(col);

                                        string sentence = string.Empty;

                                        try
                                        {
                                            sentence = hssfcell.RichStringCellValue.ToString();
                                        }
                                        catch
                                        {
                                            sentence = hssfcell.NumericCellValue.ToString();
                                        }


                                        //find header -- if we don't find it in the first 10 rows

                                        foreach (Languages varients in header.Where(x => x.Varient.ToLower() == sentence.ToLower()))
                                        {
                                            if (varients.IndexCol == "y" || varients.IndexCol == "ask")
                                            {
                                                foreach (HeaderKeys mapped_header in local_header.Where(x => x.Lang.ToLower() == varients.Header))
                                                {
                                                    headermap.Add(new HeaderMap(mapped_header.Index, col, varients.Header, true));
                                                    Console.WriteLine($"{mapped_header.Index},{col},{varients.Header},{true}");
                                                }
                                                found++;
                                            }
                                            else
                                            {
                                                foreach (HeaderKeys mapped_header in local_header.Where(x => x.Lang.ToLower() == varients.Header))
                                                {
                                                    headermap.Add(new HeaderMap(mapped_header.Index, col, varients.Header, false));
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        rown++;
                    }
                    Console.WriteLine($"Found {found}");
                    if (found > 0)
                    {
                        Console.WriteLine("Processing Tab");
                        //pickup where we left off
                        Console.WriteLine($"Found header on row {rown } Processing sheet {worksheet}");

                        foreach (HeaderMap item in headermap)
                        {
                            Console.WriteLine($"local header column = {item.Index} language {item.Lang} col with text {item.Col} index exists? {item.Indexed}");
                        }


                        /***************************************************************/

                        for (int row = rown; row < lastrow + 1; row++)
                        {


                            if ((HSSFRow)src_ws.GetRow(row) != null)
                            {

                                //if (src_ws.GetRow(row).ZeroHeight == false)
                                {
                                    int content = 0;
                                    bool exclude_row = false;

                                    foreach (HeaderMap item in headermap)
                                    {
                                        if (item.Lang.ToLower().StartsWith("comment") == false)
                                        {


                                            HSSFRow rowxls = (HSSFRow)src_ws.GetRow(row);
                                            if (rowxls.GetCell(item.Col) != null)
                                            {
                                                HSSFCell hssfcell = (HSSFCell)rowxls.GetCell(item.Col);

                                                string sentence = string.Empty;

                                                //
                                                // color exclusion would go here
                                                //

                                                try
                                                {
                                                    sentence = hssfcell.RichStringCellValue.ToString();
                                                }
                                                catch
                                                {
                                                    sentence = hssfcell.NumericCellValue.ToString();
                                                }

                                                if (sentence != "")
                                                {
                                                    content++;
                                                }
                                            }
                                        }
                                    }


                                    if (content > 0)
                                    {
                                        if (exclude_row == false)
                                        {
                                            trg_ws.Cells[trg_row, 1].Value = new FileInfo(src_file).Name;
                                            trg_ws.Cells[trg_row, 2].Value = worksheet;
                                            trg_ws.Cells[trg_row, 3].Value = row + 1;

                                            foreach (HeaderMap item in headermap)
                                            {

                                                HSSFRow rowxls = (HSSFRow)src_ws.GetRow(row);
                                                if (rowxls.GetCell(item.Col) != null)
                                                {
                                                    HSSFCell hssfcell = (HSSFCell)rowxls.GetCell(item.Col);

                                                    string segment = string.Empty;

                                                    try
                                                    {
                                                        segment = hssfcell.RichStringCellValue.ToString();
                                                    }
                                                    catch
                                                    {
                                                        segment = hssfcell.NumericCellValue.ToString();
                                                    }

                                                    segment = segment.Replace("\t", " ").Replace("\r", "");

                                                    if (keep_formatting == false)
                                                    {
                                                        trg_ws.Cells[trg_row, item.Index].Value = segment.TrimEnd();
                                                    }
                                                    else
                                                    {
                                                        richTextCopy.CopyCell(worksheet, "Sheet1", row, item.Col, trg_row, item.Index);
                                                    }



                                                }


                                                if (item.Indexed == true)
                                                {
                                                    trg_ws.Cells[trg_row, item.Index - 1].Value = ExcelUtils.ColumnIndexToColumnLetter(item.Col + 1);
                                                }
                                            }

                                            trg_row++;
                                        }
                                        else
                                        {
                                            Console.WriteLine($"Excluding Row: {row} [Ignore color]");
                                        }

                                    }
                                }


                            }


                        }




                        /***************************************************************/


                    }
                    else
                    {
                        Console.WriteLine("Skipping Tab");
                    }


                }
                else
                {
                    Console.WriteLine("Hidden");
                }
            }







            if (src_file.ToLower().EndsWith(".xls"))
            {
                target_excel.SaveAs(new FileInfo(src_file.Replace(".xls", "_Mono_Tab_Prepped.xlsx")));
            }

            if (src_file.ToLower().EndsWith(".xlsx"))
            {
                target_excel.SaveAs(new FileInfo(src_file.Replace(".xlsx", "_Mono_Tab_Prepped.xlsx")));
            }

            if (src_file.ToLower().EndsWith(".xlsm"))
            {
                target_excel.SaveAs(new FileInfo(src_file.Replace(".xlsm", "_Mono_Tab_Prepped.xlsx")));
            }


            if (keep_formatting == true)
            {
                Console.WriteLine("Finalizing colors in file");
                string current_name = string.Empty;

                if (src_file.ToLower().EndsWith(".xls"))
                    current_name = src_file.Replace(".xls", "_Mono_Tab_Prepped.xlsx");

                if (src_file.ToLower().EndsWith(".xlsx"))
                    current_name = src_file.Replace(".xlsx", "_Mono_Tab_Prepped.xlsx");

                if (src_file.ToLower().EndsWith(".xlsm"))
                    current_name = src_file.Replace(".xlsm", "_Mono_Tab_Prepped.xlsx");

                string updated_name = current_name.Replace("_Mono_Tab_Prepped.xlsx", "_Mono_Tab_Prepped_Keep_Formatting.xlsx");
                JankyFileToFix(current_name);
                File.Delete(current_name);
                //File.Move(updated_name, current_name);
            }


        }

        static void RestoreOriginalFiles()
        {
            //string language_keys = "./languages.xlsx";
            string language_keys = GetLanguageFile();

            /*
            if (!File.Exists(language_keys))
            {
                //language keys
                Console.WriteLine("Copy location to the languages.xlsx file");
                language_keys = Console.ReadLine();
                @language_keys = language_keys.Replace("\"", "");
            }
            */

            //check if this is a Directory or file
            Console.WriteLine("Copy path to folder containing ORIGINAL files to restore -=OR=- Copy and paste a SINGLE ORIGINAL file to restore");
            string @org_path_file = Console.ReadLine();

            Console.WriteLine("Copy path to folder containing Translation files -=OR=- Copy and paste a SINGLE TRANSLATION file");
            string @tra_path_file = Console.ReadLine();


            org_path_file = org_path_file.Replace("\"", "");
            tra_path_file = tra_path_file.Replace("\"", "");

            //overwrite source?


            List<string> orignal_files = new List<string>();
            List<string> translated_files = new List<string>();

            string original_path = String.Empty;
            string translated_path = String.Empty;

            //original files
            if (org_path_file.ToLower().EndsWith(".xlsx") || org_path_file.ToLower().EndsWith(".xls") || org_path_file.ToLower().EndsWith(".xlsm"))
            {
                original_path = new FileInfo(org_path_file).DirectoryName;
                orignal_files.Add(new FileInfo(org_path_file).Name);
                Console.WriteLine($"Original: {new FileInfo(org_path_file).Name}");
            }
            else
            {
                original_path = org_path_file;
                string[] original_file_names = Directory.GetFiles(original_path, "*.xls*");

                foreach (string file in original_file_names)
                {
                    orignal_files.Add(new FileInfo(file).Name);
                    Console.WriteLine($"Original: {new FileInfo(file).Name}");

                }

            }

            //translated files
            if (tra_path_file.ToLower().EndsWith(".xlsx") || tra_path_file.ToLower().EndsWith(".xls") || tra_path_file.ToLower().EndsWith(".xlsm"))
            {
                translated_path = new FileInfo(tra_path_file).DirectoryName;
                translated_files.Add(new FileInfo(tra_path_file).Name);
                Console.WriteLine($"Translation: {new FileInfo(tra_path_file).Name}");
            }
            else
            {
                translated_path = tra_path_file;
                string[] translated_file_names = Directory.GetFiles(translated_path, "*.xls*");

                foreach (string file in translated_file_names)
                {
                    translated_files.Add(new FileInfo(file).Name);
                    Console.WriteLine($"Translation: {new FileInfo(file).Name}");

                }
            }


            //assmeble!!!
            FileToRestore(original_path, translated_path, orignal_files, translated_files, language_keys);

        }


        static void FileToRestore(string original_file_path, string translated_file_path, List<string> orignal_files, List<string> translated_files, string language_keys)
        {

            //string translated_file = string.Empty;
            string worksheet = string.Empty;

            //if this was a single source file that we are explicitly providing then we should use that filename
            if (orignal_files.Count == 1 && translated_files.Count == 1)
            {

                string[] original_single_file = orignal_files.ToArray();
                string[] translated_single_file = translated_files.ToArray();


                Console.WriteLine($"We will put the translations into this {original_single_file[0]} (original) from this {translated_single_file[0]} (Translated)");
                RestoreFile(Path.Combine(original_file_path, original_single_file[0]), Path.Combine(translated_file_path, translated_single_file[0]), language_keys,false);
            }
            else
            {

                if (orignal_files.Count == 1)
                {

                    string[] original_single_file = orignal_files.ToArray();

                    //make a back-up of the original file!
                    File.Copy(Path.Combine(original_file_path, original_single_file[0]), Path.Combine(original_file_path, original_single_file[0].Replace(".xls", "_Backup.xls")));

                    foreach (string translated_file in translated_files)
                    {
                        ExcelPackage translated_excel = new ExcelPackage(new FileInfo(Path.Combine(translated_file_path, translated_file)));
                        ExcelWorksheet trans_ws = translated_excel.Workbook.Worksheets[0];

                        string original_file = string.Empty;

                        RestoreFile(Path.Combine(original_file_path, original_single_file[0]), Path.Combine(translated_file_path, translated_file), language_keys,true);

                    }

                    File.Move(Path.Combine(original_file_path, original_single_file[0]), Path.Combine(original_file_path, original_single_file[0].Replace(".xls", "_Finalized.xls")));
                    File.Move(Path.Combine(original_file_path, original_single_file[0].Replace(".xls", "_Backup.xls")), Path.Combine(original_file_path, original_single_file[0].Replace("_Backup.xls", ".xls")));

                }
                else
                {
                    foreach (string translated_file in translated_files)
                    {
                        ExcelPackage translated_excel = new ExcelPackage(new FileInfo(Path.Combine(translated_file_path, translated_file)));
                        ExcelWorksheet trans_ws = translated_excel.Workbook.Worksheets[0];

                        string original_file = string.Empty;
                        //bool process_file = false;

                        //quick test to see if the file exists
                        if (trans_ws.Cells[2, ColumnLetterToColumnIndex("A")].Value != null)
                        {
                            original_file = trans_ws.Cells[2, ColumnLetterToColumnIndex("A")].Value.ToString();

                            if (File.Exists(Path.Combine(original_file_path, original_file)) == true)
                            {
                                Console.WriteLine("File exists!");

                                if (trans_ws.Cells[2, ColumnLetterToColumnIndex("B")].Value != null)
                                {
                                    //worksheet = trans_ws.Cells[2, ColumnLetterToColumnIndex("B")].Value.ToString();
                                    //we can proccess
                                    //process_file = true;
                                    RestoreFile(Path.Combine(original_file_path, original_file), Path.Combine(translated_file_path, translated_file), language_keys,false);


                                }
                                else
                                {
                                    Console.WriteLine("It's missing the inital worksheet information... so we can't proccess it");
                                }

                            }
                            else
                            {
                                Console.WriteLine("File doesn't exist :( ");

                            }
                        }



                    }
                }




            }


        }

        static void RestoreFile(string original_file, string translated_file, string language_keys,bool overwrite)
        {
            Console.WriteLine($"Original: {original_file}\nTranslated: {translated_file}\n");
            //get our translated file
            ExcelPackage translated_excel = new ExcelPackage(new FileInfo(translated_file));
            ExcelWorksheet trans_ws = translated_excel.Workbook.Worksheets[0];

            string worksheet = trans_ws.Cells[2, ColumnLetterToColumnIndex("B")].Value.ToString();


            //get our headers
            ExcelPackage headers = new ExcelPackage(new FileInfo(language_keys));
            ExcelWorksheet header_context = headers.Workbook.Worksheets[0];
            List<RestoreMapping> restoreMappings = new List<RestoreMapping>();

            int index_col_start = 4;
            for (int col = 1; col < header_context.Dimension.Columns + 1; col++)
            {
                if (header_context.Cells[2, col].Value != null)
                {
                    string option = header_context.Cells[2, col].Value.ToString();
                    string langkey = header_context.Cells[1, col].Value.ToString();
                    switch (option)
                    {
                        case "y":
                            Console.WriteLine($"{langkey}: col target mapped - {ColumnIndexToColumnLetter(index_col_start)}");
                            restoreMappings.Add(new RestoreMapping(index_col_start, index_col_start + 1));
                            index_col_start = index_col_start + 2;

                            break;
                        case "ask":

                            bool decide = false;
                            while (decide == false)
                            {
                                Console.WriteLine($"{langkey}: col target mapped - {ColumnIndexToColumnLetter(index_col_start)}");
                                Console.WriteLine("Overwrite Source? y/n");
                                string choice = Console.ReadLine();
                                if (choice == "y")
                                {

                                    restoreMappings.Add(new RestoreMapping(index_col_start, index_col_start + 1));
                                    index_col_start = index_col_start + 2;

                                    decide = true;
                                }
                                if (choice == "n")
                                {
                                    index_col_start = index_col_start + 2;
                                    decide = true;
                                }

                            }
                            break;
                        default:
                            index_col_start++;
                            break;
                    }


                }

            }

            if (original_file.ToLower().EndsWith(".xls"))
            {
                Console.WriteLine($"This {original_file} is an Older XLS file");
                //process this using NPOI branch
                string newfile = original_file.Replace(".xls", "_finalized.xls");

                HSSFWorkbook original_excel;

                using (FileStream xls_file = new FileStream(original_file, FileMode.Open, FileAccess.Read))
                {
                    original_excel = new HSSFWorkbook(xls_file);
                }


                RichTextCopy richTextCopy = new RichTextCopy(translated_excel, original_excel);

                for (int row = 2; row < trans_ws.Dimension.Rows + 1; row++)
                {
                    if (trans_ws.Cells[row, ColumnLetterToColumnIndex("B")].Value != null)
                    {
                        worksheet = trans_ws.Cells[row, ColumnLetterToColumnIndex("B")].Value.ToString();

                        if (trans_ws.Cells[row, ColumnLetterToColumnIndex("C")].Value != null)
                        {
                            int rown = Convert.ToInt32(trans_ws.Cells[row, ColumnLetterToColumnIndex("C")].Value.ToString());

                            foreach (RestoreMapping col in restoreMappings)
                            {

                                if (trans_ws.Cells[row, col.IndexColumn].Value != null)
                                {
                                    if (trans_ws.Cells[row, col.IndexColumn].Value.ToString() != "")
                                    {
                                        int column = ColumnLetterToColumnIndex(trans_ws.Cells[row, col.IndexColumn].Value.ToString());
                                        if (trans_ws.Cells[row, col.TranslationColumn].Value != null)
                                        {
                                            richTextCopy.CopyCell(trans_ws.Name, worksheet, row, col.TranslationColumn, rown, column);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if(overwrite == false)
                {
                    using (FileStream xls_file = new FileStream(newfile, FileMode.Create, FileAccess.Write))
                    {
                        original_excel.Write(xls_file);
                    }
                }
                else
                {
                    using (FileStream xls_file = new FileStream(original_file, FileMode.Open, FileAccess.Write))
                    {
                        //overwrite the file
                        original_excel.Write(xls_file);
                       
                    }
                }
                



            }
            else
            {
                //process this using Epplus branch
                ExcelPackage original_excel = new ExcelPackage(new FileInfo(original_file));
                //ExcelWorksheet original_ws = original_excel.Workbook.Worksheets[worksheet];

                RichTextCopy richTextCopy = new RichTextCopy(translated_excel, original_excel);

                for (int row = 2; row < trans_ws.Dimension.Rows + 1; row++)
                {
                    if (trans_ws.Cells[row, ColumnLetterToColumnIndex("B")].Value != null)
                    {
                        worksheet = trans_ws.Cells[row, ColumnLetterToColumnIndex("B")].Value.ToString();

                        if (trans_ws.Cells[row, ColumnLetterToColumnIndex("C")].Value != null)
                        {
                            int rown = Convert.ToInt32(trans_ws.Cells[row, ColumnLetterToColumnIndex("C")].Value.ToString());

                            foreach (RestoreMapping col in restoreMappings)
                            {

                                if (trans_ws.Cells[row, col.IndexColumn].Value != null)
                                {
                                    if (trans_ws.Cells[row, col.IndexColumn].Value.ToString() != "")
                                    {
                                        int column = ColumnLetterToColumnIndex(trans_ws.Cells[row, col.IndexColumn].Value.ToString());
                                        if (trans_ws.Cells[row, col.TranslationColumn].Value != null)
                                        {
                                            richTextCopy.CopyCell(trans_ws.Name, worksheet, row, col.TranslationColumn, rown, column);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if(overwrite == false)
                {
                    original_excel.SaveAs(new FileInfo(original_file.Replace(".xls", "_finalized.xls")));
                }
                else
                {
                    original_excel.Save();
                }

            }


        }

        /* Fix Janky Files */

        static void FixJankyFiles()
        {
            //check if this is a Directory or file
            Console.WriteLine("Copy path to folder containing files to fix -=OR=- Copy and paste the individual file");
            string @path_file = Console.ReadLine();

            path_file = path_file.Replace("\"", "");

            if (path_file.ToLower().EndsWith(".xlsx") || path_file.ToLower().EndsWith(".xlsm"))
            {
                Console.WriteLine("Processing this is file");
                JankyFileToFix(path_file);
            }
            else
            {
                Console.WriteLine("Listing files");
                string[] files = Directory.GetFiles(path_file, "*.xls*");

                foreach (string file in files)
                {
                    Console.WriteLine(file);
                    JankyFileToFix(file);
                }

            }
        }

        static void JankyFileToFix(string file)
        {
            ExcelPackage src_excel = new ExcelPackage(new FileInfo(file));
            ExcelPackage trg_excel = new ExcelPackage();

            RichTextCopy richTextCopy = new RichTextCopy(src_excel, trg_excel);

            foreach (ExcelWorksheet ws_src in src_excel.Workbook.Worksheets)
            {
                ExcelWorksheet ws_trg = trg_excel.Workbook.Worksheets.Add(ws_src.Name);

                for (int row = 1; row < ws_src.Dimension.Rows + 1; row++)
                {
                    for (int col = 1; col < ws_src.Dimension.Columns + 1; col++)
                    {
                        //if(ws_src.Cells[row,col].Text)
                        richTextCopy.CopyCell(ws_src.Name, ws_src.Name, row, col, row, col);
                    }
                }
            }

            trg_excel.SaveAs(new FileInfo(file.Replace(".xls", "_Keep_Formatting.xls")));

        }

        /* Fix Janky Files */

        static void FixIllegalChars()
        {
            //check if this is a Directory or file
            Console.WriteLine("Copy path to folder containing files to fix -=OR=- Copy and paste the individual file");
            string @path_file = Console.ReadLine();

            path_file = path_file.Replace("\"", "");

            if (path_file.ToLower().EndsWith(".xlsx") || path_file.ToLower().EndsWith(".xlsm"))
            {
                Console.WriteLine("Processing this is file");
                IllegalCharFileToFix(path_file);
            }
            else
            {
                Console.WriteLine("Listing files");
                string[] files = Directory.GetFiles(path_file, "*.xls*");

                foreach (string file in files)
                {
                    Console.WriteLine(file);
                    IllegalCharFileToFix(file);
                }

            }
        }

        static void IllegalCharFileToFix(string file)
        {
            ExcelPackage excel_src = new ExcelPackage(new FileInfo(file));

            foreach (ExcelWorksheet ws in excel_src.Workbook.Worksheets)
            {
                for (int row = 1; row < ws.Dimension.Rows + 1; row++)
                {
                    for (int col = 1; col < ws.Dimension.Columns + 1; col++)
                    {
                        if (ws.Cells[row, col].Value != null)
                        {
                            string sentence = ws.Cells[row, col].Text;
                            if (sentence.Contains("\n"))
                            {
                                sentence = sentence.Replace("\t", " ").Replace("\r", "");
                                sentence = sentence.TrimEnd();
                                ws.Cells[row, col].Value = sentence;
                            }
                        }
                    }
                }
            }

            excel_src.SaveAs(new FileInfo(file.Replace(".xls", "_fixed.xls")));

        }

    }



    class HeaderKeys
    {
        private int index;
        private string lang;

        public HeaderKeys(int index, string lang)
        {
            this.index = index;
            this.lang = lang;
        }

        public int Index
        {
            get { return index; }
        }

        public string Lang
        {
            get { return lang; }
        }


    }

    class HeaderMap
    {
        private int index;
        private int col;
        private string lang;
        private bool indexed;

        public HeaderMap(int index, int col, string lang, bool indexed)
        {
            this.index = index;
            this.col = col;
            this.lang = lang;
            this.indexed = indexed;
        }

        public int Index
        {
            get { return index; }
        }

        public int Col
        {
            get { return col; }
        }

        public string Lang
        {
            get { return lang; }
        }

        public bool Indexed
        {
            get { return indexed; }
        }

    }

    class Languages
    {
        private string header;
        private string indexcol;
        private string varient;
        public Languages(string header, string indexcol, string varient)
        {
            this.header = header;
            this.indexcol = indexcol;
            this.varient = varient;
        }

        public string Header
        {
            get { return header; }
        }

        public string IndexCol
        {
            get { return indexcol; }
        }

        public string Varient
        {
            get { return varient; }
        }
    }

    class RestoreMapping
    {
        private int index_column;
        private int translation_column;

        public RestoreMapping(int index_column, int translation_column)
        {
            this.index_column = index_column;
            this.translation_column = translation_column;

        }

        public int IndexColumn
        {
            get { return index_column; }
        }

        public int TranslationColumn
        {
            get { return translation_column; }
        }
    }


}

